package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String FILE_OVER_NUMBER = "合計金額が10桁を超えました";
	private static final String FILE_INVALID_FILE = "の支店コードが不正です";
	private static final String FILE_INVALID_FILE_COMMODITY = "の商品コードが不正です";
	private static final String BRANCH_LST_FILE = "支店定義ファイル";
	private static final String COMMODITY_LST_FILE = "商品定義ファイル";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// エラー処理3-1
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと商品名と商品売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();


		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", BRANCH_LST_FILE)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,"^[A-Za-z0-9]{8}$", COMMODITY_LST_FILE)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();


		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}


		Collections.sort(rcdFiles);

		for(int j =0; j < rcdFiles.size() - 1; j++) {

			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(j + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}

		BufferedReader br = null;
		for(int j =0; j < rcdFiles.size(); j++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(j));

				br = new BufferedReader(fr);

				String line;
				ArrayList<String> rcdDate = new ArrayList<String>();

				while((line = br.readLine()) != null) {
					rcdDate.add(line);
				}

				//	エラー処理2-4
				if(rcdDate.size() != 3) {
					System.out.println(rcdFiles.get(j).getName() + "のフォーマットが不正です");
					return;
				}

				// エラー処理2-3

				if(!branchNames.containsKey(rcdDate.get(0))) {
					System.out.println(rcdFiles.get(j).getName() + FILE_INVALID_FILE);
					return;
				}

				if(!commodityNames.containsKey(rcdDate.get(1))) {
					System.out.println(rcdFiles.get(j).getName() + FILE_INVALID_FILE_COMMODITY);
					return;
				}


				//	エラー処理3-2
				if(!rcdDate.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(rcdDate.get(2));

				Long saleAmount =  branchSales.get(rcdDate.get(0)) + fileSale;
				// エラー処理2-2
				if(saleAmount >= 10000000000L) {
					System.out.println(FILE_OVER_NUMBER);
					return;
				}


				Long sumItem = commoditySales.get(rcdDate.get(1)) + fileSale;
				if(sumItem >= 10000000000L) {
					System.out.println(FILE_OVER_NUMBER);
					return;
				}

				branchSales.put(rcdDate.get(0), saleAmount);
				commoditySales.put(rcdDate.get(1), sumItem);

			} catch(IOException e) {

				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> name, Map<String, Long> amount,
			String maches, String error) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			// エラー処理1-1
			if(!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] splitLine = line.split(",");
				// エラー処理1-2

				if((splitLine.length != 2) || (!splitLine[0].matches(maches))) {
					System.out.println(error + "のフォーマットが不正です");
					return false;
				}

				name.put(splitLine[0], splitLine[1]);
				amount.put(splitLine[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> name, Map<String, Long> amount) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		BufferedWriter sumfw = null;

		try {
			File fileWriter = new File(path, fileName);
			FileWriter fw = new FileWriter(fileWriter);
			sumfw = new BufferedWriter(fw);

			for (String key : name.keySet()) {
				sumfw.write(key + "," + name.get(key) + "," + amount.get(key));
				sumfw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(sumfw != null) {
				try {
					sumfw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
